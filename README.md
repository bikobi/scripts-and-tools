# Scripts and tools

A collection of various scripts and tools, found here and there in times of need.

## `colors.sh`

Display terminal colors.

CREDITS: [The Linux Documentation Project](http://tldp.org/HOWTO/Bash-Prompt-HOWTO/x329.html)

## `record_bandwidth.sh`

Measure the bandwidth consumed while the script is running.

CREDITS: [Luke Smith's video: *A demonstration of modern web bloat*](https://youtu.be/cvDyQUpaFf4)

## `randomcase.py`

Randomize the upper/lower case of a string argument.

CREDITS: myself

## `slower.c`

Slow down the output of a command in the terminal.

CREDITS: [Landon Bland's answer on StackExchange](https://unix.stackexchange.com/questions/290179/how-to-slow-down-the-scrolling-of-multipage-standard-output-on-terminal/290258#290258)

## `emojis`

Use rofi menu to search 600+ emojis and copy them to the clipboard. Requires rofi, xclip and optionally dunst.

CREDITS: Adapted from [Luke Smiths's video: *dmenu tips: Emojis and more*](https://www.youtube.com/watch?v=UCEXY46t3OA)

## `loading.sh`

Display a loading animation in the terminal: `/`>`-`>`\`>`|`.

CREDITS: [chaos' answer on AskUbuntu](https://askubuntu.com/questions/623933/how-to-create-a-rotation-animation-using-shell-script/623952#623952)

## `stopwatch.sh`

Start a stopwatch in the terminal; `ctrl` + `c` to stop it, `Enter` for laps.

CREDITS: [anishsane's answer on StackOverflow](https://stackoverflow.com/a/37986910)
