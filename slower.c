#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    int delay;
    char* rem;
    if (argc > 1) {
        delay = strtol(argv[1], &rem, 10);
    } else {
        delay = 500;
    }

    char* line;
    size_t bufsize = 0;

    struct timespec ts;
    ts.tv_sec = delay / 1000;
    ts.tv_nsec = (delay % 1000) * 1000000;

    while (getline(&line, &bufsize, stdin) != -1) {
        printf("%s", line);
        nanosleep(&ts, NULL);
    }
    free(line);
}

